/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.Serializable;

/**
 *
 * @author Jarvin
 */
public class Cliente implements Serializable{
    
    
    private String Nombres;
    private String Apellidos;
    private String Cedula;
    private double Salario;
    private double Saldo;
    private double cantRetirar;
    private double cantdepositar;
    private String Ocupacion;
    private int edad;
    private double CantDespDeposito;
    private double CanteDespRetiro;

    public Cliente() {
    }

    public Cliente(String Nombres, String Apellidos, String Cedula, double Salario, double Saldo, double cantRetirar, double cantdepositar, String Ocupacion, int edad, double CantDespDeposito, double CanteDespRetiro) {
        this.Nombres = Nombres;
        this.Apellidos = Apellidos;
        this.Cedula = Cedula;
        this.Salario = Salario;
        this.Saldo = Saldo;
        this.cantRetirar = cantRetirar;
        this.cantdepositar = cantdepositar;
        this.Ocupacion = Ocupacion;
        this.edad = edad;
        this.CantDespDeposito = CantDespDeposito;
        this.CanteDespRetiro = CanteDespRetiro;
    }

    public String getNombres() {
        return Nombres;
    }

    public void setNombres(String Nombres) {
        this.Nombres = Nombres;
    }

    public String getApellidos() {
        return Apellidos;
    }

    public void setApellidos(String Apellidos) {
        this.Apellidos = Apellidos;
    }

    public String getCedula() {
        return Cedula;
    }

    public void setCedula(String Cedula) {
        this.Cedula = Cedula;
    }

    public double getSalario() {
        return Salario;
    }

    public void setSalario(double Salario) {
        this.Salario = Salario;
    }

    public double getSaldo() {
        return Saldo;
    }

    public void setSaldo(double Saldo) {
        this.Saldo = Saldo;
    }

    public double getCantRetirar() {
        return cantRetirar;
    }

    public void setCantRetirar(double cantRetirar) {
        this.cantRetirar = cantRetirar;
    }

    public double getCantdepositar() {
        return cantdepositar;
    }

    public void setCantdepositar(double cantdepositar) {
        this.cantdepositar = cantdepositar;
    }

    public String getOcupacion() {
        return Ocupacion;
    }

    public void setOcupacion(String Ocupacion) {
        this.Ocupacion = Ocupacion;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public double getCantDespDeposito() {
        return CantDespDeposito;
    }

    public void setCantDespDeposito(double CantDespDeposito) {
        this.CantDespDeposito = CantDespDeposito;
    }

    public double getCanteDespRetiro() {
        return CanteDespRetiro;
    }

    public void setCanteDespRetiro(double CanteDespRetiro) {
        this.CanteDespRetiro = CanteDespRetiro;
    }

    

    
    
    
}
