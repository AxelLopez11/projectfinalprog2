/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import views.FrameCrearCuenta;
import views.FrameCuenta;
import views.FrameDepositar;
import views.FrameEstadoCuenta;
import views.FrameModificarDatos;
import views.FrameRetirar;
import views.LoginFrame;

/**
 *
 * @author Jarvin
 */
public class MenuController implements ActionListener{

    FrameCrearCuenta fc;
    FrameCuenta c;
    FrameDepositar fd;
    FrameRetirar fr;
    FrameModificarDatos ModDate;
    FrameEstadoCuenta ec;
    LoginFrame Log;
    
    public MenuController(LoginFrame Log){
        this.Log = Log;
    }

    public MenuController(FrameCuenta c) {
        this.c = c;
    }
    
    public MenuController(FrameDepositar fd){
        this.fd = fd;
    }
    
    public MenuController(FrameCrearCuenta fc){
      
        this.fc = fc;
    }
    
    public MenuController(FrameRetirar fr){
        this.fr = fr;
    }
    
    public MenuController(FrameModificarDatos ModDate){
        this.ModDate = new FrameModificarDatos();
        this.ModDate = ModDate;
    }
    
    public MenuController(FrameEstadoCuenta ec){
        this.ec = ec;
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        switch(e.getActionCommand()){
            case "IngDeposito":
                InvokeFrameDepositar();
            break;
            case "IngRetiro":
                InvokeRetirar();
            break;
            case "IngNewCliente":
                InvokeNewCliente();
            break;
            case "ChangeDate":
                InvokeChangeDate();
            break;
            case "VerCuenta":
                InvokeEstadoCuenta();
            break;
            case "Salir":
                InvokeExit();
            break;
        }
        
    }
    public void InvokeFrameDepositar(){
        fd = new FrameDepositar();
        fd.setVisible(true);
        fd.setLocationRelativeTo(null);
        c.dispose();   
    }
    
    public void InvokeRetirar(){
        fr = new FrameRetirar();
        fr.setVisible(true);
        fr.setLocationRelativeTo(null);
        c.dispose();
    }
    
    public void InvokeNewCliente(){
        fc = new FrameCrearCuenta();
        fc.setVisible(true);
        fc.setLocationRelativeTo(null);
        c.dispose();
    }
    
    public void InvokeChangeDate(){
        ModDate = new FrameModificarDatos();
        ModDate.setVisible(true);
        ModDate.setLocationRelativeTo(null);
        c.dispose();
    }
    
    public void InvokeEstadoCuenta(){
        ec = new FrameEstadoCuenta();
        ec.setVisible(true);
        ec.setLocationRelativeTo(null);
        c.dispose();
    }
    
    public void InvokeExit(){
        Log = new LoginFrame();
        Log.setVisible(true);
        Log.setLocationRelativeTo(null);
        c.dispose();
    }
}
