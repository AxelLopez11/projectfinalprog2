/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import javax.swing.JFileChooser;
import models.Cliente;
import views.FrameCuenta;
import views.FrameDepositar;
import views.LoginFrame;
/**
 *
 * @author Miurel Castellon
 */
public class DepositarControllers implements ActionListener{
    
    FrameCuenta fc;
    FrameDepositar fd;
    JFileChooser j;
    Cliente c;
    
    
    public DepositarControllers(){
        //this.fd = new FrameDepositar();
        this.j = new JFileChooser();
        this.c = new Cliente(); 
    }
    
    
    public DepositarControllers(FrameDepositar fd){
        this.fd = fd;
        j = new JFileChooser();
        c = new Cliente();        
    }
    
    public Cliente GetCliente(){
        return c;
    }
    
    public void SetCliente(Cliente c){
        this.c = c;
    }
    
    public DepositarControllers(FrameCuenta fc){
        this.fc = fc;
    }
    
    public void actionPerformed(ActionEvent ae) {
        switch(ae.getActionCommand()){
            case "BuscarCliente":
                j.showOpenDialog(fd);
                c = ReadCliente(j.getSelectedFile());
                fd.setCliente(c);
            break;
            case "Depositar":
                fd.NuevoSaldo(c);
                WriteCliente(j.getSelectedFile());
                
            break;    
            case "Regresar":
                Regresar();
            break;
            
        
        }
    
    }
    
    public void Regresar(){
        fc = new FrameCuenta();
        fc.setVisible(true);
        fc.setLocationRelativeTo(null);
        fd.dispose();
    }
    
    public Cliente ReadCliente(File file){
        try{
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file));
            return (Cliente)ois.readObject();
            
        }
        catch(FileNotFoundException |NullPointerException | NumberFormatException ex){
            System.out.println(ex.getMessage());
        }
         catch(IOException|ClassNotFoundException ex){
            System.out.println(ex.getMessage());
        }
        return null;
    }
   public void WriteCliente(File file){
        try{
            ObjectOutputStream w = new ObjectOutputStream(new FileOutputStream(file));
            w.writeObject(c);
            w.flush();
        }
        catch(IOException | NullPointerException | NumberFormatException ex){
            System.out.println(ex.getMessage());
            System.out.println(ex.getSuppressed());
        }
    
    }
}
 

