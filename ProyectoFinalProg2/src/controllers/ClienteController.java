/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import views.FrameCrearCuenta;
import views.FrameCuenta;
import views.LoginFrame;

/**
 *
 * @author Jarvin
 */
public class ClienteController implements ActionListener{
    FrameCuenta fc;
    LoginFrame lg;
    
    public ClienteController(LoginFrame lg) {
        this.lg = lg;
         
    }
    
    public ClienteController(FrameCuenta fc){
        super();
        this.fc = fc;
    }


    @Override
    public void actionPerformed(ActionEvent ae) {
        switch(ae.getActionCommand()){
            case "Ingresar":
                lg.ValidateUser();
                
            break;    
            case "Salir":
                System.exit(0);
            break;
            
        
        }
    
    }
   
    
}
