/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import javax.swing.JFileChooser;
import models.Cliente;
import views.FrameCuenta;
import views.FrameRetirar;

/**
 *
 * @author Miurel Castellon
 */
public class RetirarControllers implements ActionListener {

    FrameRetirar fr;
    FrameCuenta fc;
    Cliente c;
    JFileChooser j;
    
    public RetirarControllers(){
        //this.fr = new FrameRetirar();
        this.fc = new FrameCuenta();
        this.c = new Cliente();
        this.j = new JFileChooser();
        
    }
    
    
    public RetirarControllers(FrameRetirar fr){
        this.fr = fr;
        j = new JFileChooser();
        c = new Cliente();     
        
    }
    
    public Cliente GetCliente(){
        return c;
    }
    
    public void SetCliente(Cliente c){
        this.c = c;
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        switch(e.getActionCommand()){
            case "Buscar":
                j.showOpenDialog(fr);
                c = ReadCliente(j.getSelectedFile());
                fr.setCliente(c);
            break;
            case "Retirar":
                fr.NewSaldo(c);
                WriteCliente(j.getSelectedFile());
            break;
            case "Regresar":
                Regresar();
            break;
        }
    }
    
    public void Regresar(){
        fc = new FrameCuenta();
        fc.setVisible(true);
        fc.setLocationRelativeTo(null);
        fr.dispose();
    }
    
    public Cliente ReadCliente(File file){
        try{
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file));
            return (Cliente)ois.readObject();
            
        }
        catch(FileNotFoundException |NullPointerException | NumberFormatException ex){
            System.out.println(ex.getMessage());
        }
         catch(IOException|ClassNotFoundException ex){
            System.out.println(ex.getMessage());
        }
        return null;
    }
    public void WriteCliente(File file){
        try{
            ObjectOutputStream w = new ObjectOutputStream(new FileOutputStream(file));
            w.writeObject(c);
            w.flush();
        }
        catch(IOException | NullPointerException | NumberFormatException ex){
            System.out.println(ex.getMessage());
            System.out.println(ex.getSuppressed());
        }
    
    }
    
}
