/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import javax.swing.JFileChooser;
import models.Cliente;
import views.FrameCrearCuenta;
import java.lang.NumberFormatException;
import views.FrameCuenta;

/**
 *
 * @author Jarvin
 */
public class UserController implements ActionListener{
    FrameCrearCuenta FrameCuenta;
    JFileChooser j;
    Cliente cliente;
    FrameCuenta fc;
    
    public UserController(FrameCrearCuenta FrameCuenta){
        super();
        this.FrameCuenta = FrameCuenta;
        j = new JFileChooser();
        cliente = new Cliente();
    }
    
    public UserController(FrameCuenta fc){
        this.fc = fc;
    }
    
    public Cliente GetCliente(){
        return cliente;
    }
    
    public void SetCliente(Cliente cliente){
        this.cliente = cliente;
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        switch(e.getActionCommand()){
            case "Registrarse":
                cliente = FrameCuenta.getCliente();
                j.showSaveDialog(FrameCuenta);
                WriteCliente(j.getSelectedFile());                
            break;
            case "Limpiar":
                FrameCuenta.Clear();
            break;
            case "Salir":
                Regresar();
            break;
        }
              
    }
    
    public void Regresar(){
        fc = new FrameCuenta();
        fc.setVisible(true);
        fc.setLocationRelativeTo(null);
        FrameCuenta.dispose();
    }
    
    public void WriteCliente(File file){
        try{
            ObjectOutputStream w = new ObjectOutputStream(new FileOutputStream(file));
            w.writeObject(cliente);
            w.flush();
        }
        catch(IOException | NullPointerException | NumberFormatException ex){
            System.out.println(ex.getMessage());
            System.out.println(ex.getSuppressed());
        }
    
    }
    
}

