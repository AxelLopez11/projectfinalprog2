/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import models.Cliente;
import views.FrameCuenta;
import views.FrameModificarDatos;

/**
 *
 * @author Jarvin
 */
public class ModDatosController implements ActionListener{
    
    FrameModificarDatos mf;
    FrameCuenta fc;
    Cliente c;
    JFileChooser j;
    DepositarControllers d;
    
    public ModDatosController(FrameModificarDatos mf){
        super();
        this.mf = mf;
        j = new JFileChooser();
        c = new Cliente();
        fc = new FrameCuenta();
    }
    
    public Cliente GetCliente(){
        return c;
    }
    
    public void SetCliente(Cliente c){
        this.c = c;
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        switch(e.getActionCommand()){
            case "Buscar":
//                mf = new FrameModificarDatos();
//                j = new JFileChooser();
//                c = new Cliente();
                j.showOpenDialog(mf);
                c = ReadCliente(j.getSelectedFile());
                mf.SetCiente(c);
            break;
            case "Aplicar":
                mf.ModDatos(c);
                WriteCliente(j.getSelectedFile());
            break;
            case "Limpiar":
                mf.Limpiar();
            break;
            case "Regresar":
                salir();
            break;    
        }
    }
    
    public void salir(){
        fc = new FrameCuenta();
        fc.setVisible(true);
        fc.setLocationRelativeTo(null);
        mf.dispose();
    }
    
    public Cliente ReadCliente(File file){
        try{
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file));
            return (Cliente)ois.readObject();
            
        }
        catch(FileNotFoundException |NullPointerException | NumberFormatException ex){
            System.out.println(ex.getMessage());
        }
         catch(IOException|ClassNotFoundException ex){
            System.out.println(ex.getMessage());
        }
        return null;
    }
   public void WriteCliente(File file){
        try{
            ObjectOutputStream w = new ObjectOutputStream(new FileOutputStream(file));
            w.writeObject(c);
            w.flush();
        }
        catch(IOException | NullPointerException | NumberFormatException ex){
            System.out.println(ex.getMessage());
            System.out.println(ex.getSuppressed());
        }
    
    }
}
