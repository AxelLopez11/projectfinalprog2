/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFileChooser;
import models.Cliente;
import views.FrameCuenta;
import views.FrameEstadoCuenta;

/**
 *
 * @author Jarvin
 */
public class VerCuentaController implements ActionListener{
    DepositarControllers d;
    Cliente c;
    FrameEstadoCuenta e;
    FrameCuenta fc;
    JFileChooser j;
    
    public VerCuentaController(){
        this.d = new DepositarControllers();
        this.c = new Cliente();
        this.e = new FrameEstadoCuenta();
        this.fc = new FrameCuenta();
        this.j = new JFileChooser();
    }
    
    public VerCuentaController(FrameEstadoCuenta e){
        this.e = e;
        this.c = new Cliente();
        this.j = new JFileChooser();
        this.d = new DepositarControllers();
    }
    
    @Override
    public void actionPerformed(ActionEvent ae) {
        switch(ae.getActionCommand()){
            case "Buscar":
                j.showOpenDialog(e);
                c = d.ReadCliente(j.getSelectedFile());
                e.SetCliente(c);
            break;
            case "Regresar":
                Salir();
                
            break;
        }
    }
    
    public void Salir(){
        fc = new FrameCuenta();
        fc.setVisible(true);
        fc.setLocationRelativeTo(null);
        e.dispose();
    }
    
    
    
}
