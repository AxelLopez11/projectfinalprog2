/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;

import controllers.UserController;
import javax.swing.JOptionPane;
import models.Cliente;

/**
 *
 * @author Jarvin
 */
public class FrameCrearCuenta extends javax.swing.JFrame {
    UserController uc;
    /**
     * Creates new form FrameCrearCuenta
     */
    public FrameCrearCuenta() {
        initComponents();
        this.setLocationRelativeTo(null);
        SetupControllers();
        setResizable(false);
    }
    
    public void SetupControllers(){
        uc = new UserController(this);
        BtnRegistrarse.addActionListener(uc);
        BtnLimpiar.addActionListener(uc);
        BtnSalir.addActionListener(uc);
    }

    public Cliente getCliente(){
        Cliente c = new Cliente();
        int LimEdad = 18;
        if (txtNombres.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Debe añadir sus nombres...!!!");
            txtNombres.requestFocus();
        } else if (txtApellidos.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Debe añadir sus apellidos...!!!");
            txtApellidos.requestFocus();
        } else if (txtCedula.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Debe proporcionar su numero de cédula...!!!");
            txtCedula.requestFocus();
        } else if (txtSalarioAct.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Debe proporcionar su salario actual...!!!");
            txtSalarioAct.requestFocus();
        } else if (txtCantInicial.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Debe depositar una cantidad inicial...!!!");
            txtCantInicial.requestFocus();
        } else if (txtOcupacion.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Debe proporcionar su trabajo actual...!!!");
            txtOcupacion.requestFocus();
        } else if (txtEdad.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Debe proporcionarnos su edad...!!!");
            txtEdad.requestFocus();
        } else if (Integer.parseInt(txtEdad.getText()) < LimEdad) {
            JOptionPane.showMessageDialog(null, "No podemos añadir cuentas de menores de edad...!!!");
            txtEdad.requestFocus();
        } else 
       
            try{
                c.setNombres(txtNombres.getText());
                c.setApellidos(txtApellidos.getText());
                c.setCedula(txtCedula.getText());
                c.setSalario(Double.parseDouble(txtSalarioAct.getText()));
                c.setSaldo(Double.parseDouble(txtCantInicial.getText()));
                c.setOcupacion(txtOcupacion.getText());
                c.setEdad(Integer.parseInt(txtEdad.getText()));
            }catch(NumberFormatException e){
                System.out.println(e);
            }
        return c;
    }
    
    public void Clear(){
        txtNombres.setText("");
        txtApellidos.setText("");
        txtCedula.setText("");
        txtSalarioAct.setText("");
        txtCantInicial.setText("");
        txtOcupacion.setText("");
        txtEdad.setText("");
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        txtSalarioAct = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        txtNombres = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtApellidos = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txtCedula = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        txtCantInicial = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        txtOcupacion = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        txtEdad = new javax.swing.JTextField();
        jPanel3 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        BtnRegistrarse = new javax.swing.JButton();
        BtnLimpiar = new javax.swing.JButton();
        BtnSalir = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jLabel5.setText("Salario Actual:");

        txtSalarioAct.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtSalarioActKeyTyped(evt);
            }
        });

        jLabel2.setText("Nombres:");

        jLabel3.setText("Apellidos:");

        jLabel4.setText("Cedula:");

        jLabel8.setText("Cantidad Inicial a depositar:");

        txtCantInicial.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtCantInicialKeyTyped(evt);
            }
        });

        jLabel6.setText("Ocupación:");

        jLabel7.setText("Edad:");

        txtEdad.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtEdadKeyTyped(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3)
                    .addComponent(jLabel4)
                    .addComponent(jLabel6))
                .addGap(53, 53, 53)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtNombres, javax.swing.GroupLayout.DEFAULT_SIZE, 167, Short.MAX_VALUE)
                    .addComponent(txtApellidos)
                    .addComponent(txtCedula)
                    .addComponent(txtOcupacion))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel8)
                    .addComponent(jLabel5)
                    .addComponent(jLabel7))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtSalarioAct, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(txtCantInicial, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 164, Short.MAX_VALUE)
                    .addComponent(txtEdad))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel2)
                        .addComponent(txtNombres, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel7)
                        .addComponent(txtEdad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(txtApellidos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4)
                            .addComponent(txtCedula, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel8)
                            .addComponent(txtCantInicial, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtSalarioAct, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(txtOcupacion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(21, Short.MAX_VALUE))
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jLabel1.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resource/image/bacnic bienvenidos.png"))); // NOI18N

        jPanel2.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        BtnRegistrarse.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resource/image/registrar.png"))); // NOI18N
        BtnRegistrarse.setText("Registrarse");

        BtnLimpiar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resource/image/clean.png"))); // NOI18N
        BtnLimpiar.setText("Limpiar");

        BtnSalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resource/image/exit.png"))); // NOI18N
        BtnSalir.setText("Salir");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(BtnRegistrarse)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(BtnLimpiar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(BtnSalir)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(BtnSalir, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(BtnRegistrarse)
                        .addComponent(BtnLimpiar)))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtEdadKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtEdadKeyTyped
        // TODO add your handling code here:
        char c = evt.getKeyChar();
        if (Character.isLetter(c)) {
            evt.consume();
            JOptionPane.showMessageDialog(this, "No puede ingresar letras", "Errror", JOptionPane.ERROR_MESSAGE);
            txtEdad.setText("");
            txtEdad.requestFocus();
        }  else if ((int) evt.getKeyChar() >= 32 && (int) evt.getKeyChar() <= 47) {
            getToolkit().beep();
            evt.consume();
            JOptionPane.showMessageDialog(this, "Caracter no valido");
            txtEdad.setCursor(null);

        } else if ((int) evt.getKeyChar() >= 58 && (int) evt.getKeyChar() <= 64) {
            getToolkit().beep();
            evt.consume();
            JOptionPane.showMessageDialog(this, "Caracter no valido");
            txtEdad.setCursor(null);

        } else if ((int) evt.getKeyChar() >= 91 && (int) evt.getKeyChar() <= 255) {
            getToolkit().beep();
            evt.consume();
            JOptionPane.showMessageDialog(this, "Caracter no valido");
            txtEdad.setCursor(null);
        }
    }//GEN-LAST:event_txtEdadKeyTyped

    private void txtCantInicialKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCantInicialKeyTyped
        // TODO add your handling code here:
        char c = evt.getKeyChar();
        if (Character.isLetter(c)) {
            evt.consume();
            JOptionPane.showMessageDialog(this, "No puede ingresar letras", "Errror", JOptionPane.ERROR_MESSAGE);
            txtCantInicial.setText("");
            txtCantInicial.requestFocus();
        }  else if ((int) evt.getKeyChar() >= 32 && (int) evt.getKeyChar() <= 47) {
            getToolkit().beep();
            evt.consume();
            JOptionPane.showMessageDialog(this, "Caracter no valido");
            txtCantInicial.setCursor(null);

        } else if ((int) evt.getKeyChar() >= 58 && (int) evt.getKeyChar() <= 64) {
            getToolkit().beep();
            evt.consume();
            JOptionPane.showMessageDialog(this, "Caracter no valido");
            txtCantInicial.setCursor(null);

        } else if ((int) evt.getKeyChar() >= 91 && (int) evt.getKeyChar() <= 255) {
            getToolkit().beep();
            evt.consume();
            JOptionPane.showMessageDialog(this, "Caracter no valido");
            txtCantInicial.setCursor(null);
        }
    }//GEN-LAST:event_txtCantInicialKeyTyped

    private void txtSalarioActKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSalarioActKeyTyped
        // TODO add your handling code here:
        char c = evt.getKeyChar();
        if (Character.isLetter(c)) {
            evt.consume();
            JOptionPane.showMessageDialog(this, "No puede ingresar letras", "Errror", JOptionPane.ERROR_MESSAGE);
            txtSalarioAct.setText("");
            txtSalarioAct.requestFocus();
        }  else if ((int) evt.getKeyChar() >= 32 && (int) evt.getKeyChar() <= 47) {
            getToolkit().beep();
            evt.consume();
            JOptionPane.showMessageDialog(this, "Caracter no valido");
            txtSalarioAct.setCursor(null);

        } else if ((int) evt.getKeyChar() >= 58 && (int) evt.getKeyChar() <= 64) {
            getToolkit().beep();
            evt.consume();
            JOptionPane.showMessageDialog(this, "Caracter no valido");
            txtSalarioAct.setCursor(null);

        } else if ((int) evt.getKeyChar() >= 91 && (int) evt.getKeyChar() <= 255) {
            getToolkit().beep();
            evt.consume();
            JOptionPane.showMessageDialog(this, "Caracter no valido");
            txtSalarioAct.setCursor(null);
        }
    }//GEN-LAST:event_txtSalarioActKeyTyped

    /**
     * @param args the command line arguments
     */

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BtnLimpiar;
    private javax.swing.JButton BtnRegistrarse;
    private javax.swing.JButton BtnSalir;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JTextField txtApellidos;
    private javax.swing.JTextField txtCantInicial;
    private javax.swing.JTextField txtCedula;
    private javax.swing.JTextField txtEdad;
    private javax.swing.JTextField txtNombres;
    private javax.swing.JTextField txtOcupacion;
    private javax.swing.JTextField txtSalarioAct;
    // End of variables declaration//GEN-END:variables

    
}
